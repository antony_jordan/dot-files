set number

" Handle tabs
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set copyindent
set smarttab

" Colours
colorscheme darkblue

" Syntax Hilight
filetype plugin on
syntax on

" Use mouse when possible
set mouse=a

" Traverse line break with arrows
set whichwrap=b,s,<,>,[,]

" Spelling
set spell
setlocal spell spelllang=en_gb

" Bracket match
set showmatch

" Searching
set hlsearch
set incsearch

" Increase buffers
set history=1000
set undolevels=1000

" Paste mode
set pastetoggle=<F2>

